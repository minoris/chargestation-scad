$fn=100;

module cutter(length, material, count,edge) {
    margin = 0.03;
    
    for ( i = [0 : count-edge] ) {
        translate([ (i*2+edge) * (length/(count*2+1)) - margin, -0.005,0]) {
            cube([length / (count*2+1) + margin*2,material+0.01,material+0.01]);
        }
    }
    
}

module powersupply() {
	translate([16.5, 0, 0]) {
			difference() {
			rotate(-90, [0,1,0]) {
				color("Silver") {
					cube([9,15,16.5]);
				}
			}
			translate([-8.25,7.5,-0.1]) {
				cylinder(h=1, r=12/2, center=true, $fn=40);
			}
		}
	}
}

module raspberry() {
	color("Green") {
		union() {
			cube([5.6,8.7,1.2]);
			cube([5.6,3,2]);
		}

	}
}

module charger() {
	color("Blue") {
		cube([12,16,4]);
	}
}


module chargerbox(width, door_open = 0.5, height = 14, material = 0.415) {
	psu_width = 15;
	psu_depth = 16.5;
	psu_offset = material + 3;
	psu_height = 9;
	psu_depthoffset = 1.3;

	shelf_height = 9;

	hole_size = psu_depth + material * 2 + psu_depthoffset - 5;

	// Front
	translate([0, -width + psu_width + psu_offset + material, 0]) {
		difference() { 
			cube([material,width,height]);
			translate([-1,material * 2, material * 2]) {
				cube([10,width - (psu_offset + psu_width + material * 4),shelf_height - material * 4]);
			}
			translate([material,0,0]) 
			rotate(90, [0,0,1]) {
				cutter(length=width, material=material, count=10, edge=1);
			}
			translate([material,material,0]) 
			rotate(90, [1,0,0])
			rotate(90, [0,0,1]) {
				cutter(length=height, material=material, count=6, edge=1);
			}
			translate([material,width,0]) 
			rotate(90, [1,0,0])
			rotate(90, [0,0,1]) {
				cutter(length=height, material=material, count=6, edge=1);
			}
			translate([material,0,shelf_height])
			rotate(90, [0,0,1])
			cutter(length=width - psu_width - psu_offset, material=material, count=4, edge=1);
		
			translate([material,width - psu_width - psu_offset,shelf_height - material])
			rotate(90, [1,0,0])
			rotate(90, [0,0,1])
			cutter(length=height-(shelf_height - material), material=material, count = 2, edge=0);
		}

	}
	
	// Door
	//translate([0, -width + psu_width + psu_offset + material, 0]) {
	translate([0, ((-material * 7)*(door_open) + ((-width + psu_width + psu_offset + material)*((1-door_open)))), 0]) {
			translate([material,material * 1, material * 1]) {
				cube([material,width - (psu_offset + psu_width + material * 2),shelf_height - material * 2]);
				translate([-material,material*3, ((shelf_height - material * 2) - 2)/2]) {
					cube([material*2,material,2]);
				}
			}
	}
	
	// Door slide
	translate([0, -width + psu_width + psu_offset + material, 0]) {
			translate([material * 2,material, 0]) {
				cube([material,width - (material * 2),material * 2]);
			}
	}

	// Back
	translate([0 + psu_depth + psu_depthoffset + material, -width + psu_width + psu_offset + material, 0]) {
		difference() {
			cube([material,width,height]);
			
			translate([material,0,0]) 
			rotate(90, [0,0,1]) {
				cutter(length=width, material=material, count=10, edge=1);
			}
			translate([material,material,0]) 
			rotate(90, [1,0,0])
			rotate(90, [0,0,1]) {
				cutter(length=height, material=material, count=6, edge=1);
			}
			translate([material,width,0]) 
			rotate(90, [1,0,0])
			rotate(90, [0,0,1]) {
				cutter(length=height, material=material, count=6, edge=1);
			}
			translate([material,0,shelf_height])
			rotate(90, [0,0,1])
			cutter(length=width - psu_width - psu_offset, material=material, count=4, edge=1);
			translate([material,width - psu_width - psu_offset,0])
			rotate(90, [1,0,0])
			rotate(90, [0,0,1])
			cutter(length=height, material=material, count = 6, edge=0);
		}
	}
	
	// Left side
	translate([0, psu_width + psu_offset, 0]) {
		difference() {
			cube([psu_depth + material * 2 + psu_depthoffset,material,height]);
			translate([psu_depthoffset + 0.6 + (psu_width - 14)/2,-1,2]) {
				cube([14,10,7]);
			}
			translate([material, material, 0])
			rotate(90, [1,0,0])
			rotate(90, [0,0,1]) {
				cutter(length=height, material=material, count=6, edge=0);
			}
			translate([psu_depth + material * 2 + psu_depthoffset, material, 0])
			rotate(90, [1,0,0])
			rotate(90, [0,0,1]) {
				cutter(length=height, material=material, count=6, edge=0);
			}

			cutter(length=psu_depth + material * 2 + psu_depthoffset, material = material, count = 8, edge = 0);
			
		}
	}

	// Right side
	translate([0, -width + psu_width + psu_offset + material, 0]) {
		difference() {
			cube([psu_depth + material * 2 + psu_depthoffset,material,height]);
			
			translate([material, material, 0])
			rotate(90, [1,0,0])
			rotate(90, [0,0,1]) {
				cutter(length=height, material=material, count=6, edge=0);
			}
			translate([psu_depth + material * 2 + psu_depthoffset, material, 0])
			rotate(90, [1,0,0])
			rotate(90, [0,0,1]) {
				cutter(length=height, material=material, count=6, edge=0);
			}

			cutter(length=psu_depth + material * 2 + psu_depthoffset, material = material, count = 8, edge = 0);
			translate([0,0,shelf_height])
			cutter(length=psu_depth + material * 2 + psu_depthoffset, material = material, count = 8, edge = 1);
		}
	}

	
	// Middle devider
	translate([0, 0, 0]) {
		difference() {
			cube([psu_depth + material * 2 + psu_depthoffset,material,height]);

			translate ([((psu_depth + material * 2 + psu_depthoffset) - hole_size)/2,-1,shelf_height + material*2 + 1]) {
				cube([hole_size,10,height - shelf_height - material]);
			}
			translate([0,-0.05,0])
			cube([material*3,10,material*2]);
			translate([0,-0.05,0])
			cube([material*2+0.1,10,shelf_height - material + 0.1]);
			translate([material*2 + psu_depth + psu_depthoffset,material,0])
			rotate(90, [1,0,0])
			rotate(90, [0,0,1])
			cutter(length=height, material=material, count = 6, edge=1);
			
			translate([material,material,shelf_height - material])
			rotate(90, [1,0,0])
			rotate(90, [0,0,1])
			cutter(length=height-(shelf_height - material), material=material, count = 2, edge=1);
			
			cutter(length=psu_depth + psu_depthoffset + material * 2, material = material, count = 8, edge = 0);
			
			translate([0,0,shelf_height])
			cutter(length=psu_depth + material * 2 + psu_depthoffset, material = material, count = 8, edge = 1);
		}
	}


	// Bottom
	translate([0, -width + psu_width + psu_offset + material, 0]) {
		difference() {
			cube([psu_depth+material * 0 + psu_depthoffset + material * 2, width, material]);
			cutter(length=psu_depth + psu_depthoffset + material * 2, material = material, count = 8, edge = 1);
			translate([0,width-psu_width-psu_offset-material,0])
			cutter(length=psu_depth + psu_depthoffset + material * 2, material = material, count = 8, edge = 1);
			translate([material,0,0]) 
			rotate(90, [0,0,1]) {
				cutter(length=width, material=material, count=10, edge=0);
			}
			translate([material*2+psu_depth + psu_depthoffset,0,0]) 
			rotate(90, [0,0,1]) {
				cutter(length=width, material=material, count=10, edge=0);
			}
			translate([0,width-material,0]) 
			cutter(length=psu_depth + psu_depthoffset + material * 2, material = material, count = 8, edge = 1);
			translate([psu_depth+psu_depthoffset+materia*2l,0,0]) 
			rotate(90, [0,0,1]) {
				cutter(length=width, material=material, count=10, edge=0);
			}
		}
	}

    
	// Lid
	translate([psu_depth + psu_depthoffset + material, -width + psu_width + psu_offset + material, height]) {
		rotate(-120, [0, -1, 0]) {
			translate([-psu_depth -psu_depthoffset -material,0,0]) {
				cube([psu_depth+material * 0 + psu_depthoffset + material * 2, width, material]);
			}
		}
	}
	
	// BatteryShelf
	translate([0, 0 - ( width - psu_width - psu_offset) + material, shelf_height]) {
		difference() {
			cube([psu_depth+material * 0 + psu_depthoffset + material * 2, width - psu_width - psu_offset, material]);
			cutter(length=psu_depth + material * 2 + psu_depthoffset, material = material, count = 8, edge = 0);
			translate([0,width - psu_width - psu_offset - material,0]) 
			cutter(length=psu_depth + material * 2 + psu_depthoffset, material = material, count = 8, edge = 0);
			
			translate([material,0,0])
			rotate(90, [0,0,1])
			cutter(length=width - psu_width - psu_offset, material=material, count=4, edge=0);
			translate([material*2+psu_depth + psu_depthoffset,0,0])
			rotate(90, [0,0,1])
			cutter(length=width - psu_width - psu_offset, material=material, count=4, edge=0);
		}
	}


	// Powersupply
	translate([psu_depth + psu_depthoffset + material, psu_offset, material + psu_height]) {
		rotate(180, [0,1,0]) {
			powersupply();
		}
	}

	// Raspberry
	translate([material * 3, material,shelf_height+0.5]) {
		color("Red") {
			rotate(-90, [1,0,0]) {
				raspberry();
			}
		}
	}

	// Charger 
	translate([3,1,shelf_height + material*2]) {
		charger();
	}
}

// projection() 
// rotate(90, [0,1,0]) // Front 
// rotate(90, [1,0,0]) // Side
scale(10)
chargerbox(30);
